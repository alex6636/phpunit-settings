<?php
namespace alexs\simple\tests;
use alexs\simple\SimpleClass;
use PHPUnit\Framework\TestCase;

class SimpleTest extends TestCase
{
    /** @var SimpleClass $SimpleClass */
    protected $SimpleClass;

    protected function setUp() {
        $this->SimpleClass = new SimpleClass();
    }

    public function testTrue() {
        $this->assertTrue($this->SimpleClass->getTrue());
    }

    public function testFalse() {
        $this->assertTrue($this->SimpleClass->getFalse());
    }
}